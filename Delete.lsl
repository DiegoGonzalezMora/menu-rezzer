integer chan;

default
{
    on_rez(integer rezz){
        llResetScript();
    }
      
      
    state_entry()
    {
        chan=(-1*(integer)("0x"+llGetSubString((string)llGetOwner(),-5,-1))-50006);
        llListen(chan,"","","");
    } 
    
    listen(integer channel, string name, key id, string msg){
        if(msg=="die"){
            llDie();
        }	
    }
     
    changed(integer change){
        if (change & CHANGED_OWNER)
            llResetScript();
    }
}    