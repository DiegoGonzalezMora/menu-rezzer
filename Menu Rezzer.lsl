/*Modify fairy coordinates [posX, posY, posZ, rotX, rotY, rotZ}*/
list sitSpread=[1.25, 1.05468, -2.09, 0, 0, -73]; 
list sitCrossed=[0.82, -0.11046, -1.10, 6, 0, -5.60]; 
list kneeling=[1.69, -0.21912, -1.065, -8, 21.44100, -172.46650]; 
list kneelSpread=[1.57, -0.29, 1.227, -27, 7, -182]; 
list spinning=[1.3, -0.65, 0.19910, 0, 0, 0]; 
list hovering=[1.36217, -0.076, -0.81726, 0, -6, -5.60]; 


/*----DONT CHANGE BELOW---*/
integer chan;
integer counter;
integer dialogChannel; 
integer dialogHandle;
key ownerKey;
key lastUser;
string user="owner";
float posX;
float posY;
float posZ;
float rot1;
float rot2;
float rot3;

getCoor(list name){
    posX=llList2Float(name, 0);
    posY=llList2Float(name, 1);
    posZ=llList2Float(name, 2);
    
    rot1=llList2Float(name, 3);
    rot2=llList2Float(name, 4);
    rot3=llList2Float(name, 5);
}

open_menu(string inputString, list inputList){
    dialogChannel = (integer)llFrand(DEBUG_CHANNEL)*-1;
    dialogHandle = llListen(dialogChannel, "", lastUser, "");
    llDialog(lastUser, inputString, inputList, dialogChannel);
}
 
close_menu(){
    llListenRemove(dialogHandle);
    dialogHandle = FALSE;
    dialogChannel = (integer)llFrand(DEBUG_CHANNEL)*-1;
}

rezz_fairy(string name){   
    
    llRezObject(name,llGetPos()+<posX, posY, posZ>*llGetRot(), ZERO_VECTOR, llEuler2Rot(<rot1, rot2, rot3>*DEG_TO_RAD/llGetLocalRot()) , 0); 
    
    counter++;
}

default
{
    state_entry()
    {             
        ownerKey = llGetOwner();
        lastUser=ownerKey;
        chan=(-1*(integer)("0x"+llGetSubString((string)llGetOwner(),-5,-1))-50006);
    }
    
    touch_start(integer num_detected)
    {
        lastUser=llDetectedKey(0);
        
        if (user=="owner" && lastUser!=ownerKey ) return;
        
        else if ( user=="group" && !llDetectedGroup(0) && lastUser != ownerKey ) return;
        
        if (dialogHandle) close_menu();
        
        open_menu("\nSelect a fairy to rezz or remove them.",
        ["Menu: "+user, "Remove all", "-", "Sit spread", "Sit crossed", "Kneeling", "Spinning", "Hovering", "Kneel spread"]);
        
    }
    
    listen(integer channel, string name, key id, string msg)
    {
        if(channel!=dialogChannel) return;
        close_menu(); 
        
        llSetTimerEvent(240);   
        
        if(msg=="Sit spread"){
            getCoor(sitSpread);
            rezz_fairy("Sitting_Fairy, legs spread");
        }
            
        else if(msg=="Sit crossed"){
            getCoor(sitCrossed);
            rezz_fairy("Sitting_Fairy  Crossed leg");
        }
            
        else if(msg=="Kneeling"){
            getCoor(kneeling);
            rezz_fairy("KNEELING Fairy");
        }
            
        else if(msg=="Spinning"){
            getCoor(spinning);
            rezz_fairy("Fair flying in a circle");
        }
        
        else if(msg=="Hovering"){
            getCoor(hovering);
            rezz_fairy("Rivendale ~*~ Hovering Fairy");
        }
            
        else if(msg=="Kneel spread"){
            getCoor(kneelSpread);
            rezz_fairy("Kneeling_Fairy, knees spread");
        }
        
        else if(msg=="Remove all"){
            llRegionSay(chan, "die");
            counter=0;
        }
        
        else if(msg=="Menu: owner"){
            if(lastUser!=ownerKey){
                llSay(0,"Owner the owner can change this setting");  
                return;  
            }
            else
                user="public";
        }
        
        else if(msg=="Menu: public"){
            if(lastUser!=ownerKey){
                llSay(0,"Owner the owner can change this setting");  
                return;  
            }
            else
                user="group";
        }
        
        else if(msg=="Menu: group"){
            if(lastUser!=ownerKey){
                llSay(0,"Owner the owner can change this setting");  
                return;  
            }
            else
                user="owner";
        }        
    }
    
    timer()
    {
        close_menu();
    }
    
    on_rez(integer start_param)
    {
        llResetScript(); 
    }
    
    changed(integer change)
    {
        if (change & (CHANGED_OWNER | CHANGED_LINK)) llResetScript();
    }
}